#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <regex.h>

int regex_replace(char **str, const char *pattern, const char *replace) {
    // replaces regex in pattern with replacement observing capture groups
    // *str MUST be free-able, i.e. obtained by strdup, malloc, ...
    // back references are indicated by char codes 1-31 and none of those chars can be used in the replacement string such as a tab.
    // will not search for matches within replaced text, this will begin searching for the next match after the end of prev match
    // returns:
    //   -1 if pattern cannot be compiled
    //   -2 if count of back references and capture groups don't match
    //   otherwise returns number of matches that were found and replaced
    //
    regex_t reg;
    unsigned int replacements = 0;
    // if regex can't commpile pattern, do nothing
    if(!regcomp(&reg, pattern, REG_EXTENDED)) {
        size_t nmatch = reg.re_nsub;
        regmatch_t m[nmatch + 1];
        const char *rpl, *p;
        // count back references in replace
        int br = 0;
        p = replace;
        while(1) {
            while(*++p > 31);
            if(*p) br++;
            else break;
        } // if br is not equal to nmatch, leave
        if(br != nmatch) {
            regfree(&reg);
            return -2;
        }
        // look for matches and replace
        char *new;
        char *search_start = *str;
        while(!regexec(&reg, search_start, nmatch + 1, m, REG_NOTBOL)) {
            // make enough room
            new = (char *)malloc(strlen(*str) + strlen(replace));
            if(!new) exit(EXIT_FAILURE);
            *new = '\0';
            strncat(new, *str, search_start - *str);
            p = rpl = replace;
            int c;
            strncat(new, search_start, m[0].rm_so); // test before pattern
            for(int k=0; k<nmatch; k++) {
                while(*++p > 31); // skip printable char
                c = *p;  // back reference (e.g. \1, \2, ...)
                strncat(new, rpl, p - rpl); // add head of rpl
                // concat match
                strncat(new, search_start + m[c].rm_so, m[c].rm_eo - m[c].rm_so);
                rpl = p++; // skip back reference, next match
            }
            strcat(new, p ); // trailing of rpl
            unsigned int new_start_offset = strlen(new);
            strcat(new, search_start + m[0].rm_eo); // trailing text in *str
            free(*str);
            *str = (char *)malloc(strlen(new)+1);
            strcpy(*str,new);
            search_start = *str + new_start_offset;
            free(new);
            replacements++;
        }
        regfree(&reg);
        // ajust size
        *str = (char *)realloc(*str, strlen(*str) + 1);
        return replacements;
    } else {
        return -1;
    }
}

int regex_first_match(char *str, const char *pattern, char* result, size_t size) {
    regex_t re;
    regmatch_t rm[2];
    if (result != NULL)
    {
        if (regcomp(&re, pattern, REG_EXTENDED) == 0)
        {
            if (regexec(&re, str, 2, rm, 0) == 0)
            {
                int len = (int)(rm[0].rm_eo - rm[0].rm_so);
                if (len > size - 1)
                    len = size - 1;
                sprintf(result, "%.*s", len, str + rm[0].rm_so);
                return 0;
            }
        }
    }
    return -1;
}

// Get local ip address
#define ip_regex "((127\.[0-9]+\.[0-9]+\.[0-9]+)|(10\.[0-9]+\.[0-9]+\.[0-9]+)|\
(172\.1[6-9]\.[0-9]+\.[0-9]+)|(172\.2[0-9]\.[0-9]+\.[0-9]+)|\
(172\.3[0-1]\.[0-9]+\.[0-9]+)|(192\.168\.[0-9]+\.[0-9]+))"
static void getDeviceIP(char* ip, size_t size)
{
    FILE *fp;
    char path[1035];
    char output[1035];

    memset(ip, 0x00, size);
    memset(path, 0x00, sizeof(path));
    memset(output, 0x00, sizeof(output));
    fp = popen("ip -br -c addr show | grep eth0", "r");
    if (fp)
    {
		while (fgets(path, sizeof(path), fp) != NULL)
		{
			if (strncmp(path, "eth0",4))
			{
				if (regex_first_match(path, ip_regex, ip, size) == 0)
					break;
			}
		}
	}

    /* close */
    pclose(fp);
}