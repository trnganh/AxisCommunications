/* Monkey HTTP Daemon
 * ------------------
 * Copyright (C) 2012, Lauri Kasanen <cand@gmx.com>
 *  Modified 2021, Axis Communications AB, Lund, Sweden
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <libmonkey.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <syslog.h>
#include <glib.h>
#include <curl.h>
#include "image.h"
#include "utils.h"
/*
 * About application
*/

enum
{
	bufsize = 4096
};
static char buf[bufsize];
static char ip[32];

static void front(char *ip)
{
	memset(buf, 0, sizeof(buf));
	sprintf(buf,
			"<html><body><center>"
			"<br><br><h1>Logika Systems</h1>"
			"<br><img src='/image.png'>"
			"<form action=/ method=post>"
			"<br><br><h>IP address: %s </h>"
			"<br><br><h>Username :  </h><input type=text  name=username value='root'>"
			"<br><br><h>Password :  </h><input type=text name=password value=''>"
			"<br><br><input type=submit>"
			"</form>"
			"</center></body></html>",
			ip);
}

static void liveview(char* user, char* pass)
{
	memset(buf, 0, sizeof(buf));
	sprintf(buf,
			"<html><head>"
 			"<meta http-equiv='Refresh' content='0; URL=http://%s:%s@%s/axis-cgi/mjpg/video.cgi'>"
			"</head></html>",
			user, pass, ip);
}

struct string
{
	char *ptr;
	size_t len;
};

void init_string(struct string *s)
{
	s->len = 0;
	s->ptr = malloc(s->len + 1);
	if (s->ptr == NULL)
	{
		fprintf(stderr, "malloc() failed\n");
		exit(EXIT_FAILURE);
	}
	s->ptr[0] = '\0';
}

size_t writefunc(void *ptr, size_t size, size_t nmemb, struct string *s)
{
	size_t new_len = s->len + size * nmemb;
	s->ptr = realloc(s->ptr, new_len + 1);
	if (s->ptr == NULL)
	{
		fprintf(stderr, "realloc() failed\n");
		exit(EXIT_FAILURE);
	}
	memcpy(s->ptr + s->len, ptr, size * nmemb);
	s->ptr[new_len] = '\0';
	s->len = new_len;

	return size * nmemb;
}

static void sendCommand(char* username, char *password)
{
	CURL *curl;
	CURLcode res;
	char command[1024];
	char authen_string[128];

	memset(buf, 0, sizeof(buf));
	memset(command, 0x00, sizeof(ip));
	memset(authen_string, 0x00, sizeof(ip));

	curl_global_init(CURL_GLOBAL_DEFAULT);
	curl = curl_easy_init();

	if (curl)
	{
		syslog(LOG_INFO, "CURL init succesful - Curl handle is created");

		sprintf(authen_string, "%s:%s", username, password);
		sprintf(command, "http://%s/axis-cgi/virtualinput/activate.cgi?schemaversion=1&port=1", ip);

		struct string s;
		init_string(&s);
		curl_easy_setopt(curl, CURLOPT_HTTPAUTH, (long)CURLAUTH_ANY);
		curl_easy_setopt(curl, CURLOPT_USERPWD, authen_string);
		curl_easy_setopt(curl, CURLOPT_URL, command);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &s);
		res = curl_easy_perform(curl);

		if (CURLE_OK == res)
		{
			sprintf(buf,
				"<html><body><textarea style='border:none;width:800px;height:600px;'>%s\n"
				"</textarea></body></html>",
				s.ptr);
		}

		free(s.ptr);

		// always cleanup
		curl_easy_cleanup(curl);

		// Once for each call you make to curl_global_init
		curl_global_cleanup();
	}

	if (strlen(buf) <= 0)
		sprintf(buf, "<html><body><center><h2>Send command fail</h2>\n"
					 "</center></body></html>");
}

static int list(const mklib_session *sr, const char *vhost, const char *url,
				const char *get, unsigned long getlen,
				const char *post, unsigned long postlen,
				unsigned int *status, const char **content, unsigned long *content_len,
				char *header)
{
	syslog(LOG_INFO, "Request url  %s", url);

	if (post)
		syslog(LOG_INFO, "Post  %s", post);
	if (get)
		syslog(LOG_INFO, "Get  %s", get);

	if (strcmp(url, "/image.png") == 0)
	{
		*content = (char *)monkey_head_png;
		*content_len = sizeof(monkey_head_png);
		sprintf(header, "Content-type: image/png");

		return MKLIB_TRUE;
	}

	if (!post)
	{
		if (strcmp(url, "/") == 0)
		{
			getDeviceIP(ip, sizeof(ip));
			front(ip);
		}
		else
		{
			return MKLIB_FALSE;
		}
	}
	else
	{
		char username[128];
		char password[128];
		memset(username, 0, 128);
		memset(password, 0, 128);

		char * token = strtok(post, "&");
		while( token != NULL ) {
			syslog(LOG_INFO, "Get param:  %s", token);
			if (strncmp(token, "username=", 9) == 0)
			{
				strcpy(username, token + 9);
				syslog(LOG_INFO, "Username:  %s", username);
			}
			else if (strncmp(token, "password=", 9) == 0)
			{
				strcpy(password, token + 9);
				syslog(LOG_INFO, "Password:  %s", password);
			}
			token = strtok(NULL, "&");
		}
		liveview(username, password);
	}

	*content = buf;
	*content_len = strlen(buf);
	sprintf(header, "Content-type: text/html");

	// TRUE here means we handled this request.
	return MKLIB_TRUE;
}

/* The callback setting interface can't check the callback for compatibility.
 * This makes sure the callback function has the right arguments. */
static cb_data listf = list;

int main(int argc, char *argv[])
{
	GMainLoop *loop;
	openlog("acap_web_app", LOG_PID | LOG_CONS, LOG_USER);
	syslog(LOG_INFO, "starting  %s", argv[0]);
	loop = g_main_loop_new(NULL, FALSE);

	// Bind to all interfaces, port 2001, default plugins, no directory.
	// Lacking the directory means that no files can be accessed, just what we want.
	// We use the data callback.
	mklib_ctx ctx = mklib_init(NULL, 0, 0, NULL);
	if (!ctx)
	{
		syslog(LOG_INFO, "mklib_init failed");
		exit(0);
	}
	syslog(LOG_INFO, "mklib_init success");

	mklib_callback_set(ctx, MKCB_DATA, listf);
	syslog(LOG_INFO, "mklib_callback_set success");

	// Start the server.
	mklib_start(ctx);
	syslog(LOG_INFO, "mklib_start success");

	g_main_loop_run(loop);

	mklib_stop(ctx);

	return 0;
}
