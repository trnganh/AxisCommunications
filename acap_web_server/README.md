## Getting started
```bash
# Build image
docker build . --build-arg ARCH=armv7hf --build-arg http_proxy --build-arg https_proxy -t acap_web_app
# Copy the result from the container image to a local directory build:
rm -rf build && docker cp $(docker create acap_web_app):/opt/monkey/acap_web_app ./build
cp build/acap_web_app_1_0_0_armv7hf.eap /Volumes/GoogleDrive/My\ Drive
```

## Browse to the web server
The Web Server can be accessed from a Web Browser eighter directly using a port number (i.e. http://mycamera:8888) or through the Apache Server in the camera using an extension to the camera web URL (i.e http://mycamera/acap_web_app/).

## C API Examples
Some C API examples are included in the app folder. To build any of the examples, use the build and install procedure as described above after making following changes to the build files:
1. app/manifest.json: Replace AppName "monkey" with the name of the example: hello, list or quiz
2. Dockerfile: Replace monkey in /usr/local/packages/monkey with the name of the example: hello, list or quiz

## License
**[Apache License 2.0](../LICENSE)**