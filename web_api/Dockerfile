ARG ARCH=armv7hf
ARG REPO=axisecp
ARG SDK=acap-native-sdk
ARG UBUNTU_VERSION=20.04
ARG VERSION=1.0

FROM ${REPO}/${SDK}:${VERSION}-${ARCH}-ubuntu${UBUNTU_VERSION}

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    autoconf \
    libtool \
    automake

WORKDIR /opt/build
RUN git clone https://github.com/curl/curl.git

# Curl lib generate
# - Source the SDK environment script to get links to cross compiler through
# - CONFIGURE_FLAGS environment variable
WORKDIR /opt/build/curl
RUN . /opt/axis/acapsdk/environment-setup* && \
    autoreconf -fi && \
    ./configure --prefix=${PWD}/build ${CONFIGURE_FLAGS} --without-ssl && \
    make && \
    make install

# Delete manual directory from curl built result
RUN rm -rf /opt/build/curl/build/share/man



# Proxy setting for curl
ARG CURL_PROXY
ENV CURL_PROXY ${CURL_PROXY}

# Build Monkey server and library
WORKDIR /opt
COPY monkey ./monkey
RUN cd monkey &&\
    . /opt/axis/acapsdk/environment-setup* &&\
    ./configure \
    --enable-shared \
    --malloc-libc \
    --prefix=/usr/local \
    --bindir=/usr/local/bin \
    --libdir=/usr/local/lib \
    --sysconfdir=/usr/local/packages/web_api/html \
    --datadir=/usr/local/packages/web_api/html \
    --mandir=/usr/local/man \
    --logdir=/tmp \
    --plugdir=/usr/local/packages/web_api/lib \
    --pidfile=/tmp/monkey.pid \
    --incdir=/usr/local/include/monkey \
    --systemddir=/usr/lib/systemd/system &&\
    make &&\
    make install

WORKDIR /opt/monkey/web_api
COPY ./app .
RUN cp /usr/local/bin/monkey .

# Copy the library to application folder
ARG BUILDDIR=/opt/build/curl/build
RUN mkdir lib && cp -r ${BUILDDIR}/lib/libcurl.so* lib

# Build examples
RUN . /opt/axis/acapsdk/environment-setup* &&\
    mkdir -p $SDKTARGETSYSROOT/usr/local &&\
    cp -r /usr/local $SDKTARGETSYSROOT/usr &&\
    mkdir -p lib &&\
    cp /usr/local/lib/libmonkey.so.1.5 /usr/local/packages/web_api/lib/*.so -t lib/ &&\
    cp -r /usr/local/packages/web_api/html . &&\
    acap-build . -a reverseproxy.conf
