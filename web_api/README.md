## Getting started
```bash
# Build image
docker build . -t web_api
# Copy the result from the container image to a local directory build:
rm -rf build && docker cp $(docker create web_api):/opt/monkey/web_api ./build
```

## Browse to the web server
The Web Server can be accessed from a Web Browser eighter directly using a port number (i.e. http://mycamera:9002) or through the Apache Server in the camera using an extension to the camera web URL (i.e http://mycamera/acap_web_app/).

## C API Examples
Some C API examples are included in the app folder. To build any of the examples, use the build and install procedure as described above after making following changes to the build files:
1. app/manifest.json: Replace AppName "monkey" with the name of the example: hello, list or quiz
2. Dockerfile: Replace monkey in /usr/local/packages/monkey with the name of the example: hello, list or quiz

## License
**[Apache License 2.0](../LICENSE)**