#ifndef VIRTUALINPUT_API_H
#define VIRTUALINPUT_API_H

#include <libmonkey.h>
#include "api.h"
#include "EventSubscribe.h"

#define GET_ALL_PORTS    	"/VirtualInput/GetAllPort"
#define GET_USED_PORT 		"/VirtualInput/GetUsePort"
#define SET_USED_PORT 		"/VirtualInput/SetUsePort"
#define GET_PULSE 		    "/VirtualInput/GetPulse"
#define SET_PULSE 		    "/VirtualInput/SetPulse"
#define GET_PORT_NAME 		"/VirtualInput/GetPortName"
#define SET_PORT_NAME 		"/VirtualInput/SetPortName"

#define VIRTUAL_INPUT_NAME_LENGTH 64

class VirtualInputAPI
{
private:
    VirtualInputAPI();

	int get_virtual_input_all_port(const mklib_session *sr, const char *vhost, const char *url,
					const char *get, unsigned long getlen,
					const char *post, unsigned long postlen,
					unsigned int *status, const char **content, unsigned long *content_len,
					char *header);

	int get_virtual_input_name(const mklib_session *sr, const char *vhost, const char *url,
					const char *get, unsigned long getlen,
					const char *post, unsigned long postlen,
					unsigned int *status, const char **content, unsigned long *content_len,
					char *header);

	int set_virtual_input_name(const mklib_session *sr, const char *vhost, const char *url,
					const char *get, unsigned long getlen,
					const char *post, unsigned long postlen,
					unsigned int *status, const char **content, unsigned long *content_len,
					char *header);

	int get_use_port(const mklib_session *sr, const char *vhost, const char *url,
					const char *get, unsigned long getlen,
					const char *post, unsigned long postlen,
					unsigned int *status, const char **content, unsigned long *content_len,
					char *header);

	int set_use_port(const mklib_session *sr, const char *vhost, const char *url,
					const char *get, unsigned long getlen,
					const char *post, unsigned long postlen,
					unsigned int *status, const char **content, unsigned long *content_len,
					char *header);

	int get_pulse(const mklib_session *sr, const char *vhost, const char *url,
					const char *get, unsigned long getlen,
					const char *post, unsigned long postlen,
					unsigned int *status, const char **content, unsigned long *content_len,
					char *header);

	int set_pulse(const mklib_session *sr, const char *vhost, const char *url,
					const char *get, unsigned long getlen,
					const char *post, unsigned long postlen,
					unsigned int *status, const char **content, unsigned long *content_len,
					char *header);
public:
    ~VirtualInputAPI();
    static VirtualInputAPI* Instance();
    void Init();

	int Process(const mklib_session *sr, const char *vhost, const char *url,
				const char *get, unsigned long getlen,
				const char *post, unsigned long postlen,
				unsigned int *status, const char **content, unsigned long *content_len,
				char *header);
};

#endif // VIRTUALINPUT_API_H