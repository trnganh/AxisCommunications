

#include "EventSubscribe.h"

EventSubscribe* EventSubscribe::Instance()
{
    static EventSubscribe   instance;   // Guaranteed to be destroyed.
                                        // Instantiated on first use.
    return &instance;
}

EventSubscribe::EventSubscribe(){
    memset(m_VirtualInputState, 0x00, sizeof(m_VirtualInputState));
    syslog(LOG_INFO, "EventSubscribe::EventSubscribe");
}

EventSubscribe::~EventSubscribe(){
    ax_event_handler_unsubscribe(m_EventHandler, m_ViHandle, NULL);
    syslog(LOG_INFO, "EventSubscribe::~EventSubscribe");
}

void EventSubscribe::Init()
{
	m_EventHandler = ax_event_handler_new();
	m_ViHandle = VirtualInputSubscription(m_EventHandler);
    syslog(LOG_INFO, "EventSubscribe::Init");
}

gboolean EventSubscribe::GetPortState(int port)
{
    if (port < VIRTUAL_INPUT_PORT_NUM)
        return m_VirtualInputState[port];
    return FALSE;
}

gboolean* EventSubscribe::GetAllPortState()
{
    return m_VirtualInputState;
}

void EventSubscribe::virtualInputCallback(guint subscription, AXEvent *event, void *data)
{
    const AXEventKeyValueSet *key_value_set;
    gboolean active;
    gint port;

    // Extract the AXEventKeyValueSet from the event
    key_value_set = ax_event_get_key_value_set(event);

    // Get the manual trigger variables of interest
    ax_event_key_value_set_get_integer(key_value_set,
                                        "port", NULL, &port, NULL);
    ax_event_key_value_set_get_boolean(key_value_set,
                                        "active", NULL, &active, NULL);

    if (port > 0 && port <= VIRTUAL_INPUT_PORT_NUM)
    {
        
        Instance()->m_VirtualInputState[port - 1] = active;
        // Print state of virtual input
        if (active)
        {
            syslog(LOG_INFO, "VirtualInput-event: Trigger on port %d is active", port);
        }
        else
        {
            syslog(LOG_INFO, "VirtualInput-event: Trigger on port %d is inactive", port);
        }
    }

    /*
   * Free the received event, n.b. AXEventKeyValueSet should not be freed
   * since it's owned by the event system until unsubscribing
   */
    ax_event_free(event);
}

void EventSubscribe::ptzmoveCallback(guint subscription, AXEvent *event, ptzmove *data)
{
    const AXEventKeyValueSet *key_value_set;
    gboolean is_moving;
    gboolean val = FALSE;
    gint channel;

    // The subscription id is not used in this example
    (void)subscription;

    // Extract the AXEventKeyValueSet from the event
    key_value_set = ax_event_get_key_value_set(event);

    // Get the state of the manual trigger port
    ax_event_key_value_set_get_integer(key_value_set,
                                       "PTZConfigurationToken", NULL, &channel, NULL);
    ax_event_key_value_set_get_boolean(key_value_set,
                                       "is_moving", NULL, &is_moving, NULL);

    // Print channel and if moving or stopped
    if (is_moving)
    {
        data->ptzchannel[channel].num_moves += 1;
        val = (data->ptzchannel[channel].num_moves == 1);
        syslog(LOG_INFO,
               "%d:ptzmove-event: PTZ channel %d started moving (%d %s)",
               data->id, channel, data->ptzchannel[channel].num_moves,
               val ? "time" : "times");
    }
    else
    {
        if (data->ptzchannel[channel].num_moves > 0)
        {
            syslog(LOG_INFO,
                   "%d:ptzmove-event: PTZ channel %d stopped moving",
                   data->id, channel);
        }
    }

    /*
   * Free the received event, n.b. AXEventKeyValueSet should not be freed
   * since it's owned by the event system until unsubscribing
   */
    ax_event_free(event);
}


guint EventSubscribe::VirtualInputSubscription(AXEventHandler *event_handler)
{
    AXEventKeyValueSet *key_value_set;
    guint subscription;

    key_value_set = ax_event_key_value_set_new();

    /* Initialize an AXEventKeyValueSet that matches the manual trigger event.
   *
   *    tns1:topic0=Device
   *    tnsaxis:topic1=IO
   *    tnsaxis:topic2=VirtualPort
   *    port=&port    <-- Subscribe to port number 1
   *    state=NULL     <-- Subscribe to all states
   */
    ax_event_key_value_set_add_key_values(key_value_set,
                                          NULL,
                                          "topic0", "tns1", "Device", AX_VALUE_TYPE_STRING,
                                          "topic1", "tnsaxis", "IO", AX_VALUE_TYPE_STRING,
                                          "topic2", "tnsaxis", "VirtualInput", AX_VALUE_TYPE_STRING,
                                          "port", NULL, NULL, AX_VALUE_TYPE_INT,
                                          "active", NULL, NULL, AX_VALUE_TYPE_BOOL,
                                          NULL);

    // Setup subscription and connect to callback function
    ax_event_handler_subscribe(event_handler,                                   // event handler
                               key_value_set,                                   // key value set
                               &subscription,                                   // subscription id
                               (AXSubscriptionCallback)virtualInputCallback,    // callback function
                               NULL,                                            // user data
                               NULL);                                           // GError

    // Free key value set
    ax_event_key_value_set_free(key_value_set);

    // Return subscription id
    syslog(LOG_INFO, "Virtual Input subscription id: %d", subscription);
    return subscription;
}

guint EventSubscribe::PTZmoveSubscription(AXEventHandler *event_handler, ptzmove *data)
{
    AXEventKeyValueSet *key_value_set;
    guint subscription;

    key_value_set = ax_event_key_value_set_new();

    /* Initialize an AXEventKeyValueSet that matches the tampering event.
   *
   *    tns1:topic0=PTZController
   * tnsaxis:topic1=Move
   *        channel=NULL    <-- Subscribe to all PTZ channels
   *      is_moving=NULL    <-- Subscribe to all values
   */
    ax_event_key_value_set_add_key_values(key_value_set,
                                          NULL,
                                          "topic0", "tns1", "PTZController", AX_VALUE_TYPE_STRING,
                                          "topic1", "tnsaxis", "Move", AX_VALUE_TYPE_STRING,
                                          "PTZConfigurationToken", NULL, NULL, AX_VALUE_TYPE_INT,
                                          "is_moving", NULL, NULL, AX_VALUE_TYPE_BOOL,
                                          NULL);

    // Setup subscription and connect to callback function
    ax_event_handler_subscribe(event_handler,                            // event handler
                               key_value_set,                            // key value set
                               &subscription,                            // subscription id
                               (AXSubscriptionCallback)ptzmoveCallback, // callback function
                               data,                                     // user data
                               NULL);                                    // GError

    // Free key value set
    ax_event_key_value_set_free(key_value_set);

    // Return subscription id
    syslog(LOG_INFO, "PTZ move subscription id: %d", subscription);
    return subscription;
}

// guint EventSubscribe::AudiotriggerSubscription(AXEventHandler *event_handler, guint token)
// {
//     AXEventKeyValueSet *key_value_set;
//     guint subscription;

//     key_value_set = ax_event_key_value_set_new();

//     /* Initialize an AXEventKeyValueSet that matches the tampering event.
//    *
//    *    tns1:topic0=AudioSource
//    * tnsaxis:topic1=TriggerLevel
//    *        channel=NULL    <-- Subscribe to all channels
//    *      triggered=NULL    <-- Subscribe to all states
//    */
//     ax_event_key_value_set_add_key_values(key_value_set,
//                                           NULL,
//                                           "topic0", "tns1", "AudioSource", AX_VALUE_TYPE_STRING,
//                                           "topic1", "tnsaxis", "TriggerLevel", AX_VALUE_TYPE_STRING,
//                                           "channel", NULL, NULL, AX_VALUE_TYPE_INT,
//                                           "triggered", NULL, NULL, AX_VALUE_TYPE_BOOL,
//                                           NULL);

//     // Setup subscription and connect to callback function
//     ax_event_handler_subscribe(event_handler,                           // event handler
//                                key_value_set,                           // key value set
//                                &subscription,                           // subscription id
//                                (AXSubscriptionCallback)commonCallback, // callback function
//                                &token,                                  // user data
//                                NULL);                                   // GError

//     // Free key value set
//     ax_event_key_value_set_free(key_value_set);

//     // Return subscription id
//     syslog(LOG_INFO, "Audio trigger subscription id: %d", subscription);
//     return subscription;
// }

// guint EventSubscribe::DaynightSubscription(AXEventHandler *event_handler, guint token)
// {
//     AXEventKeyValueSet *key_value_set;
//     guint subscription;

//     key_value_set = ax_event_key_value_set_new();

//     /* Initialize an AXEventKeyValueSet that matches the day/night event.
//    *
//    *    tns1:topic0=VideoSource
//    * tnsaxis:topic1=DayNightVision
//    * VideoSource...=NULL    <-- Subscribe to all values
//    *            day=NULL    <-- Subscribe to all states
//    */
//     ax_event_key_value_set_add_key_values(key_value_set,
//                                           NULL,
//                                           "topic0", "tns1", "VideoSource", AX_VALUE_TYPE_STRING,
//                                           "topic1", "tnsaxis", "DayNightVision", AX_VALUE_TYPE_STRING,
//                                           "VideoSourceConfigurationToken", NULL, NULL, AX_VALUE_TYPE_INT,
//                                           "day", NULL, NULL, AX_VALUE_TYPE_BOOL,
//                                           NULL);

//     // Setup subscription and connect to callback function
//     ax_event_handler_subscribe(event_handler,                           // event handler
//                                key_value_set,                           // key value set
//                                &subscription,                           // subscription id
//                                (AXSubscriptionCallback)commonCallback, // callback function
//                                &token,                                  // user data
//                                NULL);                                   // GError

//     // Free key value set
//     ax_event_key_value_set_free(key_value_set);

//     // Return subscription id
//     syslog(LOG_INFO, "Day/Night subscription id: %d", subscription);
//     return subscription;
// }

// guint EventSubscribe::ManualtriggerSubscription(AXEventHandler *event_handler, guint token)
// {
//     AXEventKeyValueSet *key_value_set;
//     gint port = 1;
//     guint subscription;

//     key_value_set = ax_event_key_value_set_new();

//     /* Initialize an AXEventKeyValueSet that matches the manual trigger event.
//    *
//    *    tns1:topic0=Device
//    * tnsaxis:topic1=IO
//    * tnsaxis:topic2=VirtualPort
//    *           port=&port    <-- Subscribe to port number 1
//    *          state=NULL     <-- Subscribe to all states
//    */
//     ax_event_key_value_set_add_key_values(key_value_set,
//                                           NULL,
//                                           "topic0", "tns1", "Device", AX_VALUE_TYPE_STRING,
//                                           "topic1", "tnsaxis", "IO", AX_VALUE_TYPE_STRING,
//                                           "topic2", "tnsaxis", "VirtualPort", AX_VALUE_TYPE_STRING,
//                                           "port", NULL, NULL, AX_VALUE_TYPE_INT,
//                                           "state", NULL, NULL, AX_VALUE_TYPE_BOOL,
//                                           NULL);

//     // Setup subscription and connect to callback function
//     ax_event_handler_subscribe(event_handler,                           // event handler
//                                key_value_set,                           // key value set
//                                &subscription,                           // subscription id
//                                (AXSubscriptionCallback)commonCallback, // callback function
//                                &token,                                  // user data
//                                NULL);                                   // GError

//     // Free key value set
//     ax_event_key_value_set_free(key_value_set);

//     // Return subscription id
//     syslog(LOG_INFO, "Manual trigger subscription id: %d", subscription);
//     return subscription;
// }


// guint EventSubscribe::TamperingSubscription(AXEventHandler *event_handler, guint token)
// {
//     AXEventKeyValueSet *key_value_set;
//     gint channel = 1;
//     guint subscription;

//     key_value_set = ax_event_key_value_set_new();

//     /* Initialize an AXEventKeyValueSet that matches the tampering event.
//    *
//    *    tns1:topic0=VideoSource
//    *    tnsaxis:topic1=Tampering
//    *    channel=&channel    <-- Subscribe to channel number 1
//    *    tampering=NULL        <-- Subscribe to all values
//    */
//     ax_event_key_value_set_add_key_values(key_value_set,
//                                           NULL,
//                                           "topic0", "tns1", "VideoSource", AX_VALUE_TYPE_STRING,
//                                           "topic1", "tnsaxis", "Tampering", AX_VALUE_TYPE_STRING,
//                                           "channel", NULL, &channel, AX_VALUE_TYPE_INT,
//                                           "tampering", NULL, NULL, AX_VALUE_TYPE_INT,
//                                           NULL);

//     // Setup subscription and connect to callback function
//     ax_event_handler_subscribe(event_handler,                           // event handler
//                                key_value_set,                           // key value set
//                                &subscription,                           // subscription id
//                                (AXSubscriptionCallback)commonCallback, // callback function
//                                &token,                                  // user data
//                                NULL);                                   // GError

//     // Free key value set
//     ax_event_key_value_set_free(key_value_set);

//     // Return subscription id
//     syslog(LOG_INFO, "Tampering subscription id: %d", subscription);
//     return subscription;
// }
