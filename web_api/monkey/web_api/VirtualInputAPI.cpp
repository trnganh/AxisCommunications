#include <libmonkey.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <syslog.h>
#include <glib.h>
#include "api.h"
#include "VirtualInputAPI.h"

static char m_buf[BUFF_SIZE];
static char m_VirtualInputName[VIRTUAL_INPUT_NAME_LENGTH][VIRTUAL_INPUT_PORT_NUM];
static char m_UsePort[64];
static char m_Pulse[64];

VirtualInputAPI *VirtualInputAPI::Instance()
{
	static VirtualInputAPI instance; // Guaranteed to be destroyed.
									 // Instantiated on first use.
	return &instance;
}

VirtualInputAPI::VirtualInputAPI()
{
	syslog(LOG_INFO, "VirtualInputAPI::VirtualInputAPI");
}

VirtualInputAPI::~VirtualInputAPI()
{

}

void VirtualInputAPI::Init()
{
	memset(m_buf, 0, BUFF_SIZE);
	memset(m_UsePort, 0, sizeof(m_UsePort));
	memset(m_Pulse, 0, sizeof(m_Pulse));
	memset(m_VirtualInputName, 0, sizeof(m_VirtualInputName));
	syslog(LOG_INFO, "VirtualInputAPI::Init");
}

int VirtualInputAPI::Process(const mklib_session *sr, const char *vhost, const char *url,
							 const char *get, unsigned long getlen,
							 const char *post, unsigned long postlen,
							 unsigned int *status, const char **content, unsigned long *content_len,
							 char *header)
{
	if (strncmp(url, GET_ALL_PORTS, strlen(GET_ALL_PORTS)) == 0)
	{
		return get_virtual_input_all_port(sr, vhost, url, get, getlen, post, postlen, status, content, content_len, header);
	}
	else if (strncmp(url, GET_PORT_NAME, strlen(GET_PORT_NAME)) == 0)
	{
		return get_virtual_input_name(sr, vhost, url, get, getlen, post, postlen, status, content, content_len, header);
	}
	if (strncmp(url, SET_PORT_NAME, strlen(SET_PORT_NAME)) == 0)
	{
		return set_virtual_input_name(sr, vhost, url, get, getlen, post, postlen, status, content, content_len, header);
	}
	else if (strncmp(url, GET_USED_PORT, strlen(GET_USED_PORT)) == 0)
	{
		return get_use_port(sr, vhost, url, get, getlen, post, postlen, status, content, content_len, header);
	}
	else if (strncmp(url, SET_USED_PORT, strlen(SET_USED_PORT)) == 0)
	{
		return set_use_port(sr, vhost, url, get, getlen, post, postlen, status, content, content_len, header);
	}
	else if (strncmp(url, GET_PULSE, strlen(GET_PULSE)) == 0)
	{
		return get_pulse(sr, vhost, url, get, getlen, post, postlen, status, content, content_len, header);
	}
	else if (strncmp(url, SET_PULSE, strlen(SET_PULSE)) == 0)
	{
		return set_pulse(sr, vhost, url, get, getlen, post, postlen, status, content, content_len, header);
	}

	return e_Handle_None;
}

int VirtualInputAPI::get_virtual_input_all_port(const mklib_session *sr, const char *vhost, const char *url,
												const char *get, unsigned long getlen,
												const char *post, unsigned long postlen,
												unsigned int *status, const char **content, unsigned long *content_len,
												char *header)
{
	gboolean *ports_state = EventSubscribe::Instance()->GetAllPortState();
	memset(m_buf, 0, BUFF_SIZE);
	sprintf(m_buf, "{ \"states\" : [%d", ports_state[0]);
	for (int i = 1; i < VIRTUAL_INPUT_PORT_NUM; i++)
	{
		sprintf(m_buf + strlen(m_buf), ", %d", ports_state[i]);
	}
	sprintf(m_buf + strlen(m_buf), "],");
	sprintf(m_buf + strlen(m_buf), "\"names\" : [\"%s\"", m_VirtualInputName[0]);
	for (int i = 1; i < VIRTUAL_INPUT_PORT_NUM; i++)
	{
		sprintf(m_buf + strlen(m_buf), ", \"%s\"", m_VirtualInputName[i]);
	}

	sprintf(m_buf + strlen(m_buf), "]}");

	*content = m_buf;
	*content_len = strlen(m_buf);
	sprintf(header, "Content-type: application/json");
	return e_Handle_OK;
}

int VirtualInputAPI::get_virtual_input_name(const mklib_session *sr, const char *vhost, const char *url,
											const char *get, unsigned long getlen,
											const char *post, unsigned long postlen,
											unsigned int *status, const char **content, unsigned long *content_len,
											char *header)
{
	if (get && getlen > 0)
	{
		int port = atoi(get);
		if (port > 0 && port <= VIRTUAL_INPUT_PORT_NUM)
		{
			memset(m_buf, 0, BUFF_SIZE);
			sprintf(m_buf,
					"{ \"state\" : %d, \"name\" : \"%s\" }",
					EventSubscribe::Instance()->GetPortState(port - 1), m_VirtualInputName[port - 1]);

			*content = m_buf;
			*content_len = strlen(m_buf);
			sprintf(header, "Content-type: application/json");
			return e_Handle_OK;
		}
	}
	return e_Handle_NG;
}

int VirtualInputAPI::set_virtual_input_name(const mklib_session *sr, const char *vhost, const char *url,
											const char *get, unsigned long getlen,
											const char *post, unsigned long postlen,
											unsigned int *status, const char **content, unsigned long *content_len,
											char *header)
{
	if (post)
	{
		std::string params = post;
		int pos = params.find("&");
		std::string port = params.substr(0, pos);
		std::string name = params.substr(pos + 1, params.length() - pos - 1);

		int iport = atoi(port.c_str());
		if (iport > 0 && iport <= VIRTUAL_INPUT_PORT_NUM &&
			name.length() < VIRTUAL_INPUT_NAME_LENGTH)
		{
			sprintf(m_VirtualInputName[iport - 1], "%s", name.c_str());
			sprintf(m_buf, "Set port:%d name:%s ok", iport, name.c_str());
			*content = m_buf;
			*content_len = strlen(m_buf);
			sprintf(header, "Content-type: text/html");
			return e_Handle_OK;
		}
	}

	return e_Handle_NG;
}

int VirtualInputAPI::get_use_port(const mklib_session *sr, const char *vhost, const char *url,
								  const char *get, unsigned long getlen,
								  const char *post, unsigned long postlen,
								  unsigned int *status, const char **content, unsigned long *content_len,
								  char *header)
{
	sprintf(m_buf, "{ \"UsePorts\" : \"%s\"}", m_UsePort);
	*content = m_buf;
	*content_len = strlen(m_buf);
	sprintf(header, "Content-type: application/json");
	return e_Handle_OK;
}

int VirtualInputAPI::set_use_port(const mklib_session *sr, const char *vhost, const char *url,
								  const char *get, unsigned long getlen,
								  const char *post, unsigned long postlen,
								  unsigned int *status, const char **content, unsigned long *content_len,
								  char *header)
{
	memset(m_UsePort, 0, sizeof(m_UsePort));
	if (post and postlen < sizeof(m_UsePort))
	{
		sprintf(m_UsePort, "%s", post);
		syslog(LOG_INFO, "Set use ports: %s", m_UsePort);

		sprintf(m_buf, "%s", "set use ports ok");
		*content = m_buf;
		*content_len = strlen(m_buf);
		sprintf(header, "Content-type: text/html");
		return e_Handle_OK;
	}

	return e_Handle_NG;
}

int VirtualInputAPI::get_pulse(const mklib_session *sr, const char *vhost, const char *url,
							   const char *get, unsigned long getlen,
							   const char *post, unsigned long postlen,
							   unsigned int *status, const char **content, unsigned long *content_len,
							   char *header)
{
	sprintf(m_buf, "{ \"Pulse\" : \"%s\"}", m_Pulse);
	*content = m_buf;
	*content_len = strlen(m_buf);
	sprintf(header, "Content-type: application/json");
	return e_Handle_OK;
}

int VirtualInputAPI::set_pulse(const mklib_session *sr, const char *vhost, const char *url,
							   const char *get, unsigned long getlen,
							   const char *post, unsigned long postlen,
							   unsigned int *status, const char **content, unsigned long *content_len,
							   char *header)
{
	memset(m_Pulse, 0, sizeof(m_Pulse));
	if (post and postlen < sizeof(m_Pulse))
	{
		sprintf(m_Pulse, "%s", post);
		syslog(LOG_INFO, "Set pulse: %s", m_Pulse);

		sprintf(m_buf, "%s", "set pulse ok");
		*content = m_buf;
		*content_len = strlen(m_buf);
		sprintf(header, "Content-type: text/html");
		return e_Handle_OK;
	}

	return e_Handle_NG;
}