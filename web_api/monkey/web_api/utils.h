#ifndef UTILS_H
#define UTILS_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <regex.h>

/*
* Replace regex pattern
*/
int regex_replace(char **str, const char *pattern, const char *replace);

/*
* Find the first matched of regex pattern
*/
int regex_first_match(char *str, const char *pattern, char* result, size_t size);

/*
* Get device local ip address
*/
void getDeviceIP(char* ip, size_t size);

/*
* Parse request 's parameter
*/
// static void parseparameter(const char* params_str, size_t len)
// {
// 	memset(params, 0, sizeof(params) * sizeof(params[0]));
// 	params_count = 0;

// 	char * token = strtok(params, "&");
// 	while( token != NULL ) {
// 		strcpy(params[params_count], token);
// 		params_count++;
// 		token = strtok(NULL, "&");
// 	}
// }

#endif // UTILS_H