#ifndef EVENTSUBSCRIBE_H
#define EVENTSUBSCRIBE_H

#include <axsdk/axevent.h>
#include <glib.h>
#include <glib-object.h>
#include <syslog.h>

#define VIRTUAL_INPUT_PORT_NUM 32

typedef enum
{
    AudioTriggerToken = 1000,
    DayNightToken,
    ManualTriggerToken,
    VirtualInputToken,
    PTZMoveToken,
    TamperingToken,
} SubscribeToken;

typedef struct
{
    guint id;
    struct
    {
        gint num_moves;
    } ptzchannel[8];
} ptzmove;

class EventSubscribe
{
private:
    EventSubscribe();
    static void virtualInputCallback(guint subscription, AXEvent *event, void *data);
    static void ptzmoveCallback(guint subscription, AXEvent *event, ptzmove *data);
	
    AXEventHandler* m_EventHandler = nullptr;
  	guint m_ViHandle = 0;
    gboolean m_VirtualInputState[VIRTUAL_INPUT_PORT_NUM];

public:
    ~EventSubscribe();
    static EventSubscribe* Instance();
    void Init();

    gboolean GetPortState(int port);
    gboolean *GetAllPortState();

    guint VirtualInputSubscription(AXEventHandler *event_handler);
    guint PTZmoveSubscription(AXEventHandler *event_handler, ptzmove *data);

    // guint AudiotriggerSubscription(AXEventHandler *event_handler, guint token);
    // guint DaynightSubscription(AXEventHandler *event_handler, guint token);
    // guint ManualtriggerSubscription(AXEventHandler *event_handler, guint token);
    // guint TamperingSubscription(AXEventHandler *event_handler, guint token);

};

#endif // EVENTSUBSCRIBE_H
