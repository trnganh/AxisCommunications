#ifndef API_H
#define API_H

#define BUFF_SIZE 4096
typedef enum {
    e_Handle_None = -1,
    e_Handle_NG = 0,
    e_Handle_OK = 1,
} E_Handle;

#endif // API_H
