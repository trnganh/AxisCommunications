## Getting started
Build the app
```bash
docker build --tag acap_video_stream .
```

Copy the result from the container image to a local directory build:
```bash
rm -rf build && docker cp $(docker create acap_video_stream):/opt/app ./build
```