/**
 * Copyright (C) 2021 Axis Communications AB, Lund, Sweden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <syslog.h>
#include <opencv2/imgproc.hpp>
#include <opencv2/video.hpp>
#include <opencv2/opencv.hpp>
#include <nadjieb/mjpeg_streamer.hpp>

#include "imgprovider.h"
#include "imgconverter.h"

using namespace cv;
using MJPEGStreamer = nadjieb::MJPEGStreamer;

int main(int argc, char *argv[])
{
    openlog("acap_video_stream", LOG_PID | LOG_CONS, LOG_USER);
    syslog(LOG_INFO, "Running video stream application");
    const unsigned int CHANNELS = 3;
    ImgProvider_t *provider = NULL;

    // Open port for stream video
    MJPEGStreamer streamer;
    streamer.start(8080);

    // The desired width and height of the BGR frame
    unsigned int width = 1280;
    unsigned int height = 720;

    // chooseStreamResolution gets the least resource intensive stream
    // that exceeds or equals the desired resolution specified above
    unsigned int streamWidth = 0;
    unsigned int streamHeight = 0;
    if (!chooseStreamResolution(width, height, &streamWidth,
                                &streamHeight))
    {
        syslog(LOG_ERR, "%s: Failed choosing stream resolution", __func__);
        exit(1);
    }

    syslog(LOG_INFO, "Creating VDO image provider and creating stream %d x %d",
           streamWidth, streamHeight);
    provider = createImgProvider(streamWidth, streamHeight, 2, VDO_FORMAT_YUV);
    if (!provider)
    {
        syslog(LOG_ERR, "%s: Failed to create ImgProvider", __func__);
        exit(2);
    }

    syslog(LOG_INFO, "Start fetching video frames from VDO");
    if (!startFrameFetch(provider))
    {
        syslog(LOG_ERR, "%s: Failed to fetch frames from VDO", __func__);
        exit(3);
    }

    uint8_t* rgbData = (uint8_t*)malloc(streamWidth * streamHeight * 4);
    if (!rgbData)
    {
        syslog(LOG_ERR, "%s: Failed to allocate rgb frame", __func__);
        exit(3);
    }

    // Create OpenCV Mats for the camera frame (nv12), the converted frame (bgr)
    // and the foreground frame that is outputted by the background subtractor
    Mat bgr_mat = Mat(streamWidth, streamHeight, CV_8UC3);
    std::vector<int> params = {cv::IMWRITE_JPEG_QUALITY, 90};

    while (true)
    {
        // Get the latest NV12 image frame from VDO using the imageprovider
        VdoBuffer *buf = getLastFrameBlocking(provider);
        if (!buf)
        {
            syslog(LOG_INFO, "No more frames available, exiting");
            exit(0);
        }
        
        uint8_t* nv12Data = (uint8_t*) vdo_buffer_get_data(buf);
        memset(rgbData, 0x00, streamWidth * streamHeight * 4);
        if (!convertCropScaleU8yuvToRGB(nv12Data, streamWidth, streamHeight,
                                        (uint8_t*) rgbData, streamHeight,
                                        streamHeight)) {
            syslog(LOG_ERR, "%s: Failed img scale/convert in "
                     "convertCropScaleU8yuvToRGB() (continue anyway)",
                     __func__);
        }

        bgr_mat.data = rgbData;
        std::vector<uchar> buff_bgr;
        cv::imencode(".jpg", bgr_mat, buff_bgr, params);
        streamer.publish("/bgr", std::string(buff_bgr.begin(), buff_bgr.end()));

        // Release the VDO frame buffer
        returnFrame(provider, buf);
    }
    return EXIT_SUCCESS;
}
