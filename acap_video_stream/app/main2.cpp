/**
 * Copyright (C) 2021 Axis Communications AB, Lund, Sweden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <syslog.h>
#include <opencv2/imgproc.hpp>
#include <opencv2/video.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/videoio.hpp>
#include <nadjieb/mjpeg_streamer.hpp>

#include "imgprovider.h"

using namespace cv;
using MJPEGStreamer = nadjieb::MJPEGStreamer;

int main(int argc, char *argv[])
{
    openlog("opencv_app", LOG_PID | LOG_CONS, LOG_USER);
    syslog(LOG_INFO, "Running video stream application");
    const unsigned int CHANNELS = 3;
    ImgProvider_t *provider = NULL;

    // Open port for stream video
    MJPEGStreamer streamer;
    streamer.start(8080);
    std::vector<int> params = {cv::IMWRITE_JPEG_QUALITY, 90};

    VideoCapture cap("http://root:BrnAWz4c@172.29.0.202/axis-cgi/mjpg/video.cgi");
    if(!cap.isOpened()){
        syslog(LOG_INFO, "Error opening video stream or file");
        return -1;
    }

    while (true)
    {
        Mat frame;
        // Capture frame-by-frame
        cap >> frame;
    
        // If the frame is empty, break immediately
        if (frame.empty())
            break;

        std::vector<uchar> buff_bgr;
        cv::imencode(".jpg", frame, buff_bgr, params);
        streamer.publish("/bgr", std::string(buff_bgr.begin(), buff_bgr.end()));
    }

    return EXIT_SUCCESS;
}
