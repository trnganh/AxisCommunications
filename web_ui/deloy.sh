#!/bin/bash
AXIS_SERVER=172.29.0.202
AXIS_USERNAME=root
DIR=$PWD
rm -rf html.tar
cd ./monkey/htdocs && tar -cf html.tar ./*
cd $DIR && mv ./monkey/htdocs/html.tar ./
ssh ${AXIS_USERNAME}@${AXIS_SERVER} "cd /usr/local/packages/monkey/html && rm -rf *"
scp -r ./html.tar ${AXIS_USERNAME}@${AXIS_SERVER}:/usr/local/packages/monkey/html/html.tar
ssh ${AXIS_USERNAME}@${AXIS_SERVER} "cd /usr/local/packages/monkey/html && tar -xvf html.tar && rm -f html.tar"