# Monkey HTTP Daemon: Makefile
# ============================
export monkey_root=$(CURDIR)
PREFIX=$(DESTDIR)/usr/local
BINDIR=$(DESTDIR)/usr/local/bin
LIBDIR=$(DESTDIR)/usr/local/lib
INCDIR=$(DESTDIR)/usr/local/include/monkey
MANDIR=$(DESTDIR)/usr/local/man
SYSCONFDIR=$(DESTDIR)/usr/local/packages/monkey/html
DATADIR=$(DESTDIR)/usr/local/packages/monkey/html
LOGDIR=$(DESTDIR)/tmp
SYSTEMDDIR=/usr/lib/systemd/system
PLUGINDIR=$(DESTDIR)/usr/local/packages/monkey/lib
VERSION=1.5.6

all: monkey.pc 
	@$(MAKE) -s -C src all
	@$(MAKE) -s -C plugins all
	@echo "  DONE"

jemalloc:
	@echo "  CC    jemalloc [all]"
	@$(MAKE) -s -C deps/jemalloc

clean:
	@(cd src; $(MAKE) clean)
	@(cd plugins; $(MAKE) clean)
	@rm -f monkey.pc

distclean:
	@(cd src; $(MAKE) distclean)

install:
	$(MAKE) -C src all
	install -d $(BINDIR)
	install -d $(LIBDIR)/pkgconfig
	install -d $(INCDIR)
	install -d $(MANDIR)/man1
	install -d $(MANDIR)/man3
	install -d $(SYSCONFDIR)
	install -d ${SYSCONFDIR}/sites
	install -d ${SYSCONFDIR}/plugins
	install -d ${DATADIR}
	install -d ${DATADIR}/imgs
	install -d ${DATADIR}/php
	install -d ${DATADIR}/docs
	install -d ${LOGDIR}
	install -d ${PLUGINDIR}
	install -m 755 bin/* $(BINDIR)
	install -m 644 ./conf/*.* $(SYSCONFDIR)
	install -m 644 src/include/*.h $(INCDIR)
	install -d $(SYSTEMDDIR)
	install -m 644 monkey.service $(SYSTEMDDIR)
	cp -r conf/plugins/auth ${SYSCONFDIR}/plugins/
	cp -r conf/plugins/cgi ${SYSCONFDIR}/plugins/
	cp -r conf/plugins/cheetah ${SYSCONFDIR}/plugins/
	cp -r conf/plugins/dirlisting ${SYSCONFDIR}/plugins/
	cp -r conf/plugins/fastcgi ${SYSCONFDIR}/plugins/
	cp -r conf/plugins/logger ${SYSCONFDIR}/plugins/
	cp -r conf/plugins/mandril ${SYSCONFDIR}/plugins/
	install -m 644 plugins/auth/*.so ${PLUGINDIR}/
	install -m 644 plugins/cgi/*.so ${PLUGINDIR}/
	install -m 644 plugins/cheetah/*.so ${PLUGINDIR}/
	install -m 644 plugins/dirlisting/*.so ${PLUGINDIR}/
	install -m 644 plugins/fastcgi/*.so ${PLUGINDIR}/
	install -m 644 plugins/liana/*.so ${PLUGINDIR}/
	install -m 644 plugins/logger/*.so ${PLUGINDIR}/
	install -m 644 plugins/mandril/*.so ${PLUGINDIR}/
	install -m 644 ./conf/sites/* ${SYSCONFDIR}/sites
	install -m 644 ./man/*.1 $(MANDIR)/man1
	install -m 644 ./man/*.3 $(MANDIR)/man3
	install -m 644 src/include/public/libmonkey.h $(INCDIR)
	-install -m 644 src/libmonkey.so.1.5 $(LIBDIR)
	-install -m 644 monkey.pc $(LIBDIR)/pkgconfig
	-ln -sf libmonkey.so.1.5 $(LIBDIR)/libmonkey.so

	install -m 644 ./htdocs/index.html $(DATADIR)
	install -m 644 ./htdocs/404.html $(DATADIR)
	install -m 644 ./htdocs/favicon.ico $(DATADIR)
	install -d $(DATADIR)/imgs
	install -m 644 ./htdocs/imgs/monkey_logo.png $(DATADIR)/imgs
	install -m 644 ./htdocs/imgs/info_pic.jpg $(DATADIR)/imgs
	install -d $(DATADIR)/css
	install -m 644 ./htdocs/css/bootstrap.min.css $(DATADIR)/css

	### Install My app
	install -d $(DATADIR)/myapp
	cd ./htdocs/myapp && find . -type f -exec install -Dm 644 "{}" "$(DATADIR)/myapp/{}" \;

	### Install AXVIT
	install -d $(DATADIR)/axvit
	cd ./htdocs/axvit && find . -type f -exec install -Dm 644 "{}" "$(DATADIR)/axvit/{}" \;

	@echo
	@echo  " Running Monkey :"
	@echo  " ----------------"
	@echo
	@echo  "  # /usr/local/bin/monkey"
	@echo
	@echo  "  For more help use '-h' option"
	@echo

monkey.pc:
	@sed -e "s@PREFIX@/usr/local@" -e "s@LIBDIR@/usr/local/lib@" -e "s@INCDIR@/usr/local/include/monkey@" 		-e "s@VERSION@1.5.6@" monkey.pc.in > monkey.pc

