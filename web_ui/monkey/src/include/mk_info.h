#ifndef MK_INFO_H
#define MK_INFO_H

#define OS "Linux"

#define __MONKEY__            1
#define __MONKEY_MINOR__      5
#define __MONKEY_PATCHLEVEL__ 6

#define MONKEY_VERSION (__MONKEY__ * 10000 \
                        __MONKEY_MINOR__ * 100 \
                        __MONKEY_PATCHLEVEL__)
#define VERSION          "1.5.6"
#define MK_BUILD_CMD     "--enable-shared --malloc-libc --prefix=/usr/local --bindir=/usr/local/bin --libdir=/usr/local/lib --sysconfdir=/usr/local/packages/monkey/html --datadir=/usr/local/packages/monkey/html --mandir=/usr/local/man --logdir=/tmp --plugdir=/usr/local/packages/monkey/lib --pidfile=/tmp/monkey.pid --incdir=/usr/local/include/monkey --systemddir=/usr/lib/systemd/system"
#define MK_BUILD_UNAME   "Linux buildkitsandbox 5.10.47-linuxkit #1 SMP Sat Jul 3 21:51:47 UTC 2021 x86_64 x86_64 x86_64 GNU/Linux"
#define MONKEY_PATH_CONF "/usr/local/packages/monkey/html"
#define PLUGDIR "/usr/local/packages/monkey/lib"

#endif
