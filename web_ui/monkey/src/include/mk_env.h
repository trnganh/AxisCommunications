#ifndef MK_ENV_H
#define MK_ENV_H
#define CC "arm-linux-gnueabihf-gcc  -mthumb -mfpu=neon -mfloat-abi=hard -mcpu=cortex-a9 -fstack-protector-strong  -O2 -D_FORTIFY_SOURCE=2 -Wformat -Wformat-security -Werror=format-security --sysroot=/opt/axis/acapsdk/sysroots/armv7hf -L/opt/axis/acapsdk/sysroots/armv7hf/usr/lib"
#endif
