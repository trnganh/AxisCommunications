$(document).ready(function () {
    for (var i = 1; i <= 32; i++) {
        $('.ports-content').append(
            `<div class="row col-md-3 mt-5">
            <label>Port ${i < 10 ? '0' + i : i} : </label>
            <label class="switch">
            <input name="port" id="p${i}" type="checkbox">
            <span class="slider round"></span>
            </label>
        </div>`
        )
    }
    $('input[name=port]').change(function (e) {
        var target = false;
        if ($(this).is(':checked')) {
            target = true;
            console.log("Active " + this.id);
        } else {
            target = false;
            console.log("Inactive " + this.id);
        }
        // Cancel UI change state
        $(this).prop("checked", !target);

        // Call API and wait response from server
        var port = parseInt(this.id.substr(1), 0);
        $.ajax(`http://${location.hostname}/axis-cgi/virtualinput/${target ? "activate" : "deactivate"}.cgi?schemaversion=1&port=${port}`, {
            type: 'GET',
            success: function (data) {
                // console.log("Success change state");
            },
            error: function (error) {
                // console.log("Error change state");
            }
        });
    });
    setInterval(updateStates, 1000); // last arg is in milliseconds
});

function updateStates() {
    $.ajax(`http://${location.hostname}/web_api/VirtualInput/GetAllPort`, {
        type: 'GET',
        dataType: "json",
        success: function (data) {
            if (data.states) {
                for (var i = 0; i < data.states.length; i++) {
                    $(`#p${i+1}`).prop("checked", data.states[i] == 1);
                }
            }
        },
        error: function (error) {
            // console.log("Error: " + error);
        }
    });
}
