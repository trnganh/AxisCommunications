const GET_ALL_PORTS = `http://${location.hostname}/web_api/VirtualInput/GetAllPort`

const GET_USE_PORTS = `http://${location.hostname}/web_api/VirtualInput/GetUsePort`
const SET_USE_PORTS = `http://${location.hostname}/web_api/VirtualInput/SetUsePort`

const GET_PULSE = `http://${location.hostname}/web_api/VirtualInput/GetPulse`
const SET_PULSE = `http://${location.hostname}/web_api/VirtualInput/SetPulse`

const GET_PORT_NAME = `http://${location.hostname}/web_api/VirtualInput/GetPortName`
const SET_PORT_NAME = `http://${location.hostname}/web_api/VirtualInput/SetPortName`

var UsePorts = []
var UsePortsStr = ""

// Update Use VI when press enter or lost focus
const addVIManual = (idx, name, state) => {
    var html = `
    <div class="col-12 d-flex justify-content-center mb-2">
        <div class="col-md-9 px-2">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">VI ${idx}</span>
                </div>
                <input id="txt${idx}" type="text" class="form-control" aria-describedby="basic-addon1" value="${name}">
            </div>
        </div>
        <div class="col-md-3 px-2 d-flex justify-content-left">
            <div class="row p-md-1">
                <label class="switch">
                    <input name="port" id="cb${idx}" ${state === 1 ? "checked" : ""} type="checkbox" onchange="onCheckBoxChange(${idx})">
                    <span class="slider round"></span>
                </label>
            </div>
            <button id="btn${idx}" type="button" class="btn btn-success mx-4" onclick="doPulse(${idx})"
                style="width: 32px;height: 32px; border-radius: 16px;"></button>
        </div>
    </div>`

    $('#cardManual').append(html);
}

const updateUseVI = () => {
    if (UsePortsStr === $("#inputUseVI").val())
        return;

    const parseUsePort = (data) => {
        UsePorts = [];
        var str = $("#inputUseVI").val()
        str.split(',').forEach(element => {
            if (element.includes('-')) {
                var e = element.split('-')
                var nmin = parseInt(e[0])
                var nmax = parseInt(e[1])
                if (nmin >= 1 && nmax <= 32) {
                    for (let i = nmin; i <= nmax; i++) {
                        UsePorts.push(i);
                    }
                }
            } else {
                var val = parseInt(element);
                if (val >= 1 && val <= 32) {
                    UsePorts.push(val);
                }
            }
        });

        console.log(UsePorts)
        $('#cardManual').html("");
        UsePorts.forEach(idx => {
            let name = data.names[idx-1];
            let state = data.states[idx-1];
            addVIManual(idx, name, state);
        })

        UsePortsStr = str;
    }

    $.ajax(GET_ALL_PORTS, {
        type: 'GET',
        dataType: "json",
        success: function (data) {
            if (data) {
                parseUsePort(data);
            }
        },
        error: function (error) {
            console.log("Get all port info error");
        }
    });
};

function addLog(log) {
    var d = new Date,
    dformat = [ d.getFullYear(),
                d.getMonth()+1,
                d.getDate(),
               ].join('-') + ' ' +
              [d.getHours(),
               d.getMinutes(),
               d.getSeconds()].join(':') + '.' +
               d.getMilliseconds();
    $('#log').append('<div>' + dformat + ' ' + log + '</div>');
}

function addLogChangeState(idx, state)
{   
    let log;
    if (state) {
        log = `VI ${idx}` + " State Inactive --> Active ";
    } else {
        log = `VI ${idx}` + " State Active --> Inactive ";
    }
    addLog(log);
}

function doPulse(idx) {
    // Active port
    changeState(idx, true)
    $("#cb" + idx).prop('checked', true);

    // Wait for pulse (sec) and Inactive
    var pulse = $("#inputPulse").val();
    var pulsef = parseFloat(pulse)
    setTimeout(function () {
        changeState(idx, false)
        $("#cb" + idx).prop('checked', false);
    }, pulsef * 1000);
}

function getUsePort() {
    return new Promise((resolve, reject) => {
        $.ajax(GET_USE_PORTS, {
            type: 'GET',
            dataType: "json",
            success: function (data) {
                if (data.UsePorts) {
                    $("#inputUseVI").val(data.UsePorts);
                }
                resolve(data)
            },
            error: function (error) {
                console.log("Get use port error");
                reject(error)
            }
        });
    });
}

function setUsePort() {
    $.ajax(SET_USE_PORTS, {
        type: 'POST',
        data: $("#inputUseVI").val(),
        success: function (data) {
            console.log("Success: " + data);
        },
        error: function (error) {
            console.log("Set use port error");
        }
    });
}

function setPulse() {
    $.ajax(SET_PULSE, {
        type: 'POST',
        data: $("#inputPulse").val(),
        success: function (data) {
            console.log("Success: " + data);
        },
        error: function (error) {
            console.log("Set pulse error");
        }
    });
}

function getPulse() {
    return new Promise((resolve, reject) => {
        $.ajax(GET_PULSE, {
            type: 'GET',
            dataType: "json",
            success: function (data) {
                if (data.Pulse) {
                    $("#inputPulse").val(data.Pulse);
                }
                resolve(data)
            },
            error: function (error) {
                console.log("Get pulse error");
                reject(error)
            }
        });
    });
}

function setPortName(idx, name) {
    $.ajax(SET_PORT_NAME, {
        type: 'POST',
        data: `${idx}&${name}`,
        success: function (data) {
            console.log("setPortName success: " + data);
        },
        error: function (error) {
            console.log("setPortName error");
        }
    });
}

function changeState(idx, target) {
       // Call API and wait response from server
    $.ajax(`http://${location.hostname}/axis-cgi/virtualinput/${target ? "activate" : "deactivate"}.cgi?schemaversion=1&port=${idx}`, {
        type: 'GET',
        dataType: "xml",
        success: function (xml) {
            var tag = target ? "ActivateSuccess" : "DeactivateSuccess"
            var viResponse = $(xml).find('VirtualInputResponse').first().find("Success").first()
                    .find(tag).first().find("StateChanged").first()[0].firstChild.data
            if (viResponse.toLowerCase().includes("true"))
            {
                addLogChangeState(idx, target);
                // change status success
                return;
            }
        },
        error: function (error) {
            console.log(error)
        }
    }); 
}

function onCheckBoxChange(idx) {
    var target = false;
    if ($("#cb" + idx).is(':checked')) {
        target = true;
    } else {
        target = false;
    }

    changeState(idx, target);
}

function Save()
{
    setPulse();
    setUsePort();

    UsePorts.forEach(idx => {
        let name = $("#txt" + idx).val();
        setPortName(idx, name);
    })
}

function Restore()
{
    getPulse();
    getUsePort()
        .then((data) => {
            updateUseVI();
        })
        .catch((error) => {
            // console.log(error)
        });
}

$(document).ready(function () {
    $("#inputUseVI").focusout(updateUseVI);
    $("#inputUseVI").on('keypress',(e) => { if(e.which == 13) updateUseVI(); });
    
    Restore();
});



