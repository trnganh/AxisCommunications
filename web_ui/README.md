## Getting started
```bash
# Build image
docker build . -t web_ui
# Copy the result from the container image to a local directory build:
rm -rf build && docker cp $(docker create web_ui):/opt/monkey/examples ./build
```

## Browse to the web server
The Web Server can be accessed from a Web Browser eighter directly using a port number (i.e. http://mycamera:9001) or through the Apache Server in the camera using an extension to the camera web URL (i.e http://mycamera/web_ui/).

## C API Examples
Some C API examples are included in the app folder. To build any of the examples, use the build and install procedure as described above after making following changes to the build files:
1. app/manifest.json: Replace AppName "monkey" with the name of the example: hello, list or quiz
2. Dockerfile: Replace monkey in /usr/local/packages/monkey with the name of the example: hello, list or quiz

## License
**[Apache License 2.0](../LICENSE)**

########
docker run -v $PWD/app:/opt/app -it web_ui:latest

eap-install.sh 172.29.0.202 BrnAWz4c install