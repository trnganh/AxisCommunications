## Getting started

These instructions will guide you on how to execute the code. Below is the structure and scripts used in the example:

```bash
acap_initial_tasks
├── app
│   ├── LICENSE
│   ├── Makefile
│   ├── manifest.json
│   └── acap_initial_tasks.c
├── Dockerfile
└── README.md
```

* **app/LICENSE**        - File containing the license conditions.
* **app/Makefile**       - Makefile containing the build and link instructions for building the ACAP3 application.
* **app/manifest.json    - Defines the application and its configuration.
* **app/acap_initial_tasks.c** - Example application.
* **Dockerfile**         - Docker file with the specified Axis toolchain and API container to build the example specified.
* **README.md**          - Step by step instructions on how to run the example.

### How to run the code

Below is the step by step instructions on how to execute the program. So basically starting with the generation of the .eap file to running it on a device:

#### Build the application

Standing in your working directory run the following commands:

> [!IMPORTANT]
> *Depending on the network you are connected to.
The file that needs those settings is:* ~/.docker/config.json. *For
reference please see: <https://docs.docker.com/network/proxy/> and a
[script for Axis device here](../../FAQs.md#HowcanIset-upnetworkproxysettingsontheAxisdevice?).*

```bash
docker build --tag acap_initial_tasks:1.0 .
```

Copy the result from the container image to a local directory build:
```bash
rm -rf build && docker cp $(docker create acap_initial_tasks:1.0):/opt/app ./build
cp -f build/acap_initial_tasks_1_0_0_armv7hf.eap /Volumes/GoogleDrive/My\ Drive
```

#### Install your application

Installing your application on an Axis device is as simple as:

Browse to the following page (replace <axis_device_ip> with the IP number of your Axis device)

```bash
http://<axis_device_ip>/#settings/apps
```

*Goto your device web page above > Click on the tab **App** in the device GUI > Click Add **(+)** sign and browse to
the newly built **acap_initial_tasks_1_0_0_armv7hf.eap** > Click **Install** > Run the application by enabling the **Start** switch*

Application acap_initial_tasks is now available as an application on the device.
