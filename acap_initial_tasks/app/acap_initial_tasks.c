/**
 * Copyright (C) 2021, Axis Communications AB, Lund, Sweden
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

/**
 * brief This example illustrates how to use external curl library in an
 * ACAP application
 *
 * This example shows how curl library can be used to fetch a file from the
 * URL and store the content locally.
 *
 */

#include <syslog.h>
#include <curl.h>

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "utils.h"

#define ip_regex "((127\.[0-9]+\.[0-9]+\.[0-9]+)|(10\.[0-9]+\.[0-9]+\.[0-9]+)|\
(172\.1[6-9]\.[0-9]+\.[0-9]+)|(172\.2[0-9]\.[0-9]+\.[0-9]+)|\
(172\.3[0-1]\.[0-9]+\.[0-9]+)|(192\.168\.[0-9]+\.[0-9]+))"

int main(void)
{
    CURL *curl;
    CURLcode res;
    char ip[32];
    char command[1024];
    FILE *fp;
    char path[1035];
    char output[1035];

    openlog(NULL, LOG_PID, LOG_USER);
    syslog(LOG_INFO, "=========== Start app ===========");

    memset(ip, 0x00, sizeof(ip));
    memset(path, 0x00, sizeof(path));
    memset(output, 0x00, sizeof(output));
    fp = popen("ip -br -c addr show | grep eth0", "r");
    if (fp == NULL)
    {
        syslog(LOG_PERROR, "Failed to run command\n");
        exit(1);
    }

    /* Read the output a line at a time - output it. */
    syslog(LOG_INFO, "<-- cmd -->");
    while (fgets(path, sizeof(path), fp) != NULL)
    {
        if (strncmp(path, "eth0",4))
        {
            if (regex_first_match(path, ip_regex, ip, sizeof(ip)) != 0)
            {
                syslog(LOG_PERROR, "Error in getting IP");
                exit(1);
            }
            else
                break;
        }
    }

    /* close */
    pclose(fp);

    /* display result */
    syslog(LOG_INFO, "IP address: %s\n", ip);

    // This function sets up the program environment that libcurl needs
    curl_global_init(CURL_GLOBAL_DEFAULT);
    curl = curl_easy_init();

    if (curl)
    {
        syslog(LOG_INFO, "CURL init succesful - Curl handle is created");

        memset(command, 0x00, sizeof(ip));
        sprintf(command, "http://%s/axis-cgi/virtualinput/activate.cgi?schemaversion=1&port=1", ip);

        curl_easy_setopt(curl, CURLOPT_HTTPAUTH, (long)CURLAUTH_ANY);
        curl_easy_setopt(curl, CURLOPT_USERPWD, "root:BrnAWz4c");
        curl_easy_setopt(curl, CURLOPT_URL, command);
        res = curl_easy_perform(curl);

        if (CURLE_OK == res)
        {
            char *ct;
            /* ask for the content-type */
            res = curl_easy_getinfo(curl, CURLINFO_CONTENT_TYPE, &ct);

            if ((CURLE_OK == res) && ct)
                syslog(LOG_INFO, "We received Content-Type: %s\n", ct);
            else
                syslog(LOG_PERROR, "curl get info fail\n");
        }
        else
            syslog(LOG_PERROR, "curl perform fail\n");

        // always cleanup
        curl_easy_cleanup(curl);

        // Once for each call you make to curl_global_init
        curl_global_cleanup();
    }

    syslog(LOG_INFO, "=========== Finish app ===========");
    return 0;
}
